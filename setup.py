import os
from cx_Freeze import setup, Executable

os.environ['TCL_LIBRARY'] = "D:\\Users\\chand\\AppData\\Local\\Programs\\Python\\Python36\\tcl\\tcl8.6"
os.environ['TK_LIBRARY'] = "D:\\Users\\chand\\AppData\\Local\\Programs\\Python\\Python36\\tcl\\tk8.6"

include_files = [r"D:\Users\chand\AppData\Local\Programs\Python\Python36\DLLs\tcl86t.dll",
                 r"D:\Users\chand\AppData\Local\Programs\Python\Python36\DLLs\tk86t.dll"]

setup(
    name = "Viv's File Fixer",
    version = "0.1",
    author = "Chandler",
    author_email = "",
    options = {"build_exe": {"includes": ["tkinter", "os", "mimetypes", "shutil"], "include_files": include_files}},
    executables = [Executable("main.py", base = "Win32GUI")],
    )
