from tkinter.filedialog import askdirectory
import os
import mimetypes
from shutil import copyfile

path = askdirectory()

def scan(folder):
    it = os.scandir(folder)

    for entry in it:
        if entry.is_file() and (mimetypes.guess_type(entry.path)[0] == 'image/jpeg'):
            copyfile(entry.path, os.path.join(path, os.path.basename(os.path.dirname(entry.path))+'.jpg'))
        if entry.is_dir():
            scan(entry.path)

def top_scan(folder):
    it = os.scandir(folder)
    for entry in it:
        if entry.is_dir():
         scan(entry.path)



if __name__ == "__main__":
    top_scan(path)